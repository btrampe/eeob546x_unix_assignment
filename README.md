#Benjamin Trampe
##UNIX ASSIGNMENT - EEOB546X

###Objective
There are two main goals of this assignment:
1. Inspect the data files which are `fang_et_al_genotypes.txt` & `snp_position.txt` and 2. Process and format the above data files analysis in the future. 

This `README.md` will serve as a step by step documentation of my undertaking to inspect and process this data files 

The first I will undertake the inspection of the data files. All results and code will be included under _Data Inspection_. 


##_Data Inspection_
The assignment asks for the file size, number of lines, and number of columns. 

#####Code for file size 

`$ du -sh fang_et_al_genotypes.txt snp_position.txt` 

The `du` command displays the file size for each file. The `-sh` denote that the file is human readable and displays entry for each specified file. 

####Results for file size

The file size for `fang_et_al_genotypes.txt` is 11 megabytes and `snp_position.txt` is 84 kilobytes.

```
11M	fang_et_al_genotypes.txt
84K	snp_position.txt
```
#### Code for the number of lines

`$ wc -l fang_et_al_genotypes.txt snp_position.txt`

The `wc` command gives the word count and the `-l` option produces the number of lines in each of the files. 

#### Results for the number of lines

`fang_et_al_genotypes.txt` contains 2,783 lines.
`snp_position.txt`contains 984 lines. 

```
2783 fang_et_al_genotypes.txt
984 snp_position.txt
3767 total
```
#### Code for number of columns

```
$ awk -F '\t' '{print NF; exit}' fang_et_al_genotypes.txt
$ awk -F '\t' '{print NF; exit}' snp_position.txt 
```
The **AWK** programming language is used here to find the number of columns. The -F option denotes the file type. The file type used was tab-delimited file type. Then, it prints the number of records which is the number of columns. This is done for both of the files. 

#### Results for the number of columns

The number of columns for `fang_et_al_genotypes.txt` is 986 and for `snp_position.txt` is 15. 

```
986 fang_et_al_genotypes.txt
15 snp_position.txt
```

#### Code for viewing files 

`$ cut -f 1-20 fang_et_al_genotypes.txt | column -t | head`

Due to the large file structure of `fang_et_al_genotypes.txt`, the `cut` command was used to reduce the size of print out in future steps. I chose to print out first 20 columns of the file. The command `head` was use to print out the first 10 rows. The `column` command was use to tidy up the print out and make it easier to read. 

` $ column -t snp_position.txt | head snp_position.txt` 

Due to the relative small file structure of `snp_position.txt`, the `cut` command was not used. I proceeded to use the `column` and `head` command to view the files. 

#### Results for viewing files

##### `fang_et_al_genotypes.txt`

```
Sample_ID    JG_OTU                    Group  abph1.20  abph1.22  ae1.3  ae1.4  ae1.5  an1.4  ba1.6  ba1.9  bt2.5  bt2.7  bt2.8  Fea2.1  Fea2.5  id1.3  lg2.11  lg2.2  pbf1.1
SL-15        T-aust-1                  TRIPS  ?/?       ?/?       T/T    G/G    T/T    C/C    ?/?    G/G    ?/?    A/A    ?/?    C/C     A/A     T/T    C/C     A/A    ?/?
SL-16        T-aust-2                  TRIPS  ?/?       ?/?       T/T    ?/?    T/T    C/C    A/G    G/G    ?/?    A/A    ?/?    C/C     A/A     T/T    C/C     A/A    T/T
SL-11        T-brav-1                  TRIPS  ?/?       ?/?       T/T    G/G    T/T    ?/?    G/G    G/G    C/C    A/A    ?/?    ?/?     A/A     T/T    C/C     A/A    T/T
SL-12        T-brav-2                  TRIPS  ?/?       ?/?       T/T    G/G    T/T    C/C    G/G    G/G    C/C    A/A    ?/?    ?/?     A/A     T/T    C/C     A/A    T/T
SL-18        T-cund                    TRIPS  ?/?       ?/?       T/T    G/G    T/T    C/C    ?/?    G/G    ?/?    A/A    ?/?    C/C     A/A     T/T    C/C     A/A    T/T
SL-2         T-dact-1                  TRIPS  ?/?       ?/?       T/T    G/G    T/T    C/C    A/G    G/G    ?/?    A/A    ?/?    C/C     A/A     T/T    C/C     A/A    T/T
SL-4         T-dact-2                  TRIPS  ?/?       ?/?       T/T    G/G    T/T    C/C    G/G    G/G    ?/?    A/A    ?/?    C/C     A/A     T/T    C/C     A/A    T/T
SL-6         T-dact-3                  TRIPS  ?/?       ?/?       T/T    G/G    T/T    C/C    ?/?    G/G    ?/?    A/A    ?/?    ?/?     A/A     T/T    C/C     A/A    T/T
SL-7         T-dact-4                  TRIPS  ?/?       ?/?       T/T    G/G    T/T    C/C    G/G    G/G    ?/?    A/A    ?/?    C/C     A/A     T/T    C/C     A/A    T/T 
```
##### `snp_position.txt`
```
SNP_ID	cdv_marker_id	Chromosome	Position	alt_pos	mult_positions	amplicon	cdv_map_feature.name	gene	candidate/random	Genaissance_daa_id	Sequenom_daa_id	count_amplicons	count_cmf	count_gene
abph1.20	5976	2	27403404			abph1	AB042260	abph1	candidate	8393	10474	1	1	1
abph1.22	5978	2	27403892			abph1	AB042260	abph1	candidate	8394	10475	0	0	0
ae1.3	6605	5	167889790			ae1	ae1	ae1	candidate	8395	10477	1	1	1
ae1.4	6606	5	167889682			ae1	ae1	ae1	candidate	8396	10478	0	0	0
ae1.5	6607	5	167889821			ae1	ae1	ae1	candidate	8397	10479	0	0	0
an1.4	5982	1	240498509			an1	an1	an1	candidate	8398	10481	1	1	1
ba1.6	3463	3	181362952			ba1	AY753892	ba1	candidate	8399	10482	1	0	1
ba1.9	3466	3	181362666			ba1	AY753892	ba1	candidate	8400	10483	0	0	0
bt2.5	5983	4	66290049			bt2	bt2	bt2	candidate	8401	10486	1	1	1
```
After viewing the files, I can see that `fang_et_al_genotypes.txt` contains all of the samples that were genotyped and the SNP calls for each loci. Also, the `snp_position.txt` provides information about the SNPs like SNP_ID, Chromosome, and Position. 

Based on this information, the SNP_ID information is the only information that is the same for both 

##_Data Processing_

After viewing and describing the files, the next step is the data processing to get them formatted for analysis. 

###1.Separating genotype file by groups for Maize and Teosinte

`$ awk 'NR==1; $3=="ZMMIL" || $3=="ZMMLR" || $3=="ZMMMR" ' fang_et_al_genotypes.txt > Maize_genotypes.txt`

The code above is looking at column 3 in the `fang_et_al_genotypes.txt` for ZMMIL, ZMMLR, and ZMMMR. After it has identified the groups of interest, it takes the records containing those groups and adds them to a new file named `Maize_genotypes.txt`.

`$ awk 'NR==1; $3=="ZMPBA" || $3=="ZMPIL" || $3=="ZMPJA" ' fang_et_al_genotypes.txt > Teosinte_genotypes.txt`

The code above is looking at column 3 in the `fang_et_al_genotypes.txt` for ZMPBA, ZMPIL, and ZMPJA. After it has identified the groups of interest, it takes the records containing those groups and adds them to a new file named `Teosinte_genotypes.txt`.

###2.Transposing the Maize and Teosinte files

`$ awk -f transpose.awk Maize_genotypes.txt > transposed_maize_genotypes.txt`
`$ awk -f transpose.awk Teosinte_genotypes.txt > transposed_teosinte_genotypes.txt`

The code was use to transpose each of the data sets for formatting in future steps. 

###3.Removing unnecessary rows (JD_OTU and Group) from Transposed Maize and Teosinte files

`$ sed '2,3 d' transposed_maize_genotypes.txt > removedgroups_transposed_maize_genotypes.txt`
`$ sed '2,3 d' transposed_teosinte_genotypes.txt > removedgroups_transposed_teosinte_genotypes.txt`

The code was use to remove 2 lines (2,3) which were JD_OTU and Group that were not nescessary. The new files for each maize and teosinte were created with `removedgroup` as the new prefix for the files. 

###4.Renamed Sample ID to SNP ID using `sed`

`$ sed 's/Sample_ID/SNP_ID/' removedgroups_transposed_maize_genotypes.txt > rjoin_transposed_maize_genotypes.txt`
`$ sed 's/Sample_ID/SNP_ID/' removedgroups_transposed_teosinte_genotypes.txt > rjoin_transposed_teosinte_genotypes.txt`

The files were updated to correct the SNP_ID from Sample_ID. The files were renamed during this change. rjoin - ready to join the files. 

###5.Removing unnecessary columns from `snp_position.txt` to only include SNP_ID, Chromosome, and Position

`$ cut -f 1,3,4 snp_position.txt > snp_position_condensedchromosomeposition.txt`

The code cut columns SNP_ID(1), Chromosome(3), and Position(4), then created a new file named `snp_position_condensedchromosomeposition.txt`. The numbers in the parentheses are the columns that were cut from `snp_position.txt`file.

###6.Sorting and Verifying the files by SNP Position, Maize Genotype, and Teosinte Genotype files for joining 

`$ awk 'NR == 1; NR > 1 {print $0 | "sort -f -k1"}' snp_position_condensedchromosomeposition.txt > rjoin_snp_position_condensedchromosomeposition.txt`

`$ awk 'NR == 1; NR > 1 {print $0 | "sort -f -k1"}' rjoin_transposed_maize_genotypes.txt > rjoin_sorted_transposed_maize_genotypes.txt`

`$ awk 'NR == 1; NR > 1 {print $0 | "sort -f -k1"}' rjoin_transposed_teosinte_genotypes.txt > rjoin_sorted_transposed_teosinte_genotypes.txt`

This sorted each of the files by the first column which was SNP_ID. The option -f was use used to ignore upper and lower case letters due naming of the SNPs. The awk code in each of these keeps the header at the top and doesn't sort the header, then proceeds to sort the data.

`$ wc -l rjoin_snp_position_condensedchromosomeposition.txt`
`$ wc -l rjoin_sorted_transposed_maize_genotypes.txt`
`$ wc -l rjoin_sorted_transposed_teosinte_genotypes.txt`

The command `wc` with option -l was used to verify that all of the files had the same number of lines. They all had the same number of lines, which was 984 for each file.

###7. Merging SNP and Genotype files using `join`

`$ join -t $'\t' -1 1 -2 1 rjoin_snp_position_condensedchromosomeposition.txt rjoin_sorted_transposed_maize_genotypes.txt > maize_SNP_genotype_combined.txt`

`$ join -t $'\t' -1 1 -2 1 rjoin_snp_position_condensedchromosomeposition.txt rjoin_sorted_transposed_teosinte_genotypes.txt > teosinte_SNP_genotype_combined.txt`

The genotype file for each maize and teosinte was merged with SNP position file. The column that used to join was SNP_ID column in both files. Since the files are tab delimited, the `-t` option needs to be used to produce the correct output. 

###8. Creating directories for formatted files for analysis

`$ mkdir maize_genotypes_formatted`
`$ mkdir teosinte_genotypes_formatted`
`$ mkdir intermediate_files_unix_assignment`

This step is to make new directories that are formatted for maize and teosinte for future data analysis. All of the files made up to this point are intermediate files needed to get to this point or step. I used `formatted` in the file to denote the files are formatted for the analysis. 

###9. Replacing the missing data from ? to - using `sed`

`$ sed 's/?/-/g' maize_SNP_genotype_combined.txt > maize_SNP_genotype_combined_hyphen.txt`
`$ sed 's/?/-/g' teosinte_SNP_genotype_combined.txt >teosinte_SNP_genotype_combined_hyphen.txt`

Currently the files ending in `_combined.txt` are formatted to display the missing data as question marks(?). This uses `sed` to format all of the missing data to hyphens(-). All of the newly formatted files with the change end in `_hyphen.txt`. The option `g` is use to change all of missing data instead of the first value. 

###10. Viewing the files see if the proper changes were accomplished. 

`$ cut -f 1-5 maize_SNP_genotype_combined.txt | column -t`
`$ cut -f 1-5 teosinte_SNP_genotype_combined.txt | column -t`
`$ cut -f 1-5 maize_SNP_genotype_combined_hyphen.txt | column -t`
`$ cut -f 1-5 teosinte_SNP_genotype_combined_hyphen.txt | column -t`

The first two lines of code were ran before replacing the data to see what needed to be changes needed to be done for the missing data. The last two lines verify that the changes for the missing data were correct. This allows for the evaluation of how to code and format the files for unknown and multiple positions. 

###11. Multiple SNP position files created for maize and teosinte genotypes

`$ awk 'NR==1; $2 ~ /multiple/ || $3 ~ /multiple/' maize_SNP_genotype_combined.txt > maize_genotypes_formatted/maize_multiple_positions_genotype.txt`
`$ awk 'NR==1; $2 ~ /multiple/ || $3 ~ /multiple/' teosinte_SNP_genotype_combined.txt > teosinte_genotypes_formatted/teosinte_multiple_positions_genotype.txt`

This created files that contained `multiple` in the chromosome column(2) or in position column(3). These files contain all of the genotype data for the individuals, as well SNPs that had `multiple` in either of the two columns. 

`$ awk 'NR==1; $2 ~ /multiple/ || $3 ~ /multiple/' rjoin_snp_position_condensedchromosomeposition.txt > maize_genotypes_formatted/maize_multiple_positions_SNP.txt`
`$ awk 'NR==1; $2 ~ /multiple/ || $3 ~ /multiple/' rjoin_snp_position_condensedchromosomeposition.txt > teosinte_genotypes_formatted/teosinte_multiple_positions_SNP.txt`

This created files that contained This created files that contained `multiple` in the chromosome column(2) or in position column(3). These files only contain SNPs that had `multiple` in either of the two columns. 

###12. Unknown SNP position files created for maize and teosinte genotypes

`$ awk 'NR==1; $2 ~ /unknown/ || $3 ~ /unknown/' maize_SNP_genotype_combined.txt > maize_genotypes_formatted/maize_unknown_positions_genotype.txt`
`$ awk 'NR==1; $2 ~ /unknown/ || $3 ~ /unknown/' teosinte_SNP_genotype_combined.txt > teosinte_genotypes_formatted/teosinte_unknown_positions_genotype.txt`

This created files that contained `unknown` in the chromosome column(2) or in position column(3). These files contain all of the genotype data for the individuals, as well SNPs that had `unknown` in either of the two columns. 

`$ awk 'NR==1; $2 ~ /unknown/ || $3 ~ /unknown/' rjoin_snp_position_condensedchromosomeposition.txt > maize_genotypes_formatted/maize_unknown_positions_SNP.txt`
`$ awk 'NR==1; $2 ~ /unknown/ || $3 ~ /unknown/' rjoin_snp_position_condensedchromosomeposition.txt > teosinte_genotypes_formatted/teosinte_unknown_positions_SNP.txt`

This created files that contained `unknown` in the chromosome column(2) or in position column(3). These files only contain SNPs that had `unknown` in either of the two columns. 

###13.Removing SNPs that have multiple or unknown position data

`$ grep -vwE "(unknown|multiple)" maize_SNP_genotype_combined.txt > removed_SNPS_maize_SNP_genotype_combined.txt`
`$ grep -vwE "(unknown|multiple)" maize_SNP_genotype_combined_hyphen.txt > removed_SNPS_maize_SNP_genotype_combined_hyphen.txt`
`$ grep -vwE "(unknown|multiple)" teosinte_SNP_genotype_combined.txt > removed_SNPS_teosinte_SNP_genotype_combined.txt`
`$ grep -vwE "(unknown|multiple)" teosinte_SNP_genotype_combined_hyphen.txt > removed_SNPS_teosinte_SNP_genotype_combined_hyphen.txt`

This remove all of the records that had SNPs that multiple or unknown positions. `grep` was use to search for the pattern of unknown or multiple. This leaves 940 lines or 940 records in each file. The reasoning for removing the SNPs would it would interfere with ordering of SNPs in ascending or descending order. Therefore they were removed. 

###14. Dividing the files into the chromosome for maize and teosinte 

`head -n 1 && tail -n +2` was use to keep the header from sorting when applying the sorting command. 

####Maize genotype files 

`$ for i in {1..10}; do awk 'NR==1; $2=='$i'' removed_SNPS_maize_SNP_genotype_combined.txt | (head -n 1 && tail -n +2 | sort -k3,3n) > maize_genotypes_formatted/maize_chr"$i"_genotypes_questionmark_ascending.txt; done`

Divides the file into each individual chromosome, then sorts it in ascending order. This missing data is formatted as question marks (?).

`$ for i in {1..10}; do awk 'NR==1; $2=='$i'' removed_SNPS_maize_SNP_genotype_combined_hyphen.txt | (head -n 1 && tail -n +2 | sort -k3,3nr) > maize_genotypes_formatted/maize_chr"$i"_genotypes_hyphen_descending.txt; done`

Divides the file into each individual chromosome, then sorts it in descending order. This missing data is formatted as hyphens(-).

####Teosinte genotype files

`$ for i in {1..10}; do awk 'NR==1; $2=='$i'' removed_SNPS_teosinte_SNP_genotype_combined.txt | (head -n 1 && tail -n +2 | sort -k3,3n) > teosinte_genotypes_formatted/teosinte_chr"$i"_genotypes_questionmark_ascending.txt; done`

Divides the file into each individual chromosome, then sorts it in ascending order. This missing data is formatted as question marks (?).

`$ for i in {1..10}; do awk 'NR==1; $2=='$i'' removed_SNPS_teosinte_SNP_genotype_combined_hyphen.txt | (head -n 1 && tail -n +2 | sort -k3,3nr) > teosinte_genotypes_formatted/teosinte_chr"$i"_genotypes_hyphen_descending.txt; done`

Divides the file into each individual chromosome, then sorts it in descending order. This missing data is formatted as hyphens(-).

###15. Clean and organize directory of intermediate files.

`$ mv *.txt intermediate_files_unix_assignment/` 

All intermediate files were moved to the directory called `intermediate_files_unix_assignment` using wildcard.






